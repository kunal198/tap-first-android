package faris.tbi.tapfirst;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import faris.tbi.tapfirst.main.activity.MainActivity;
import faris.tbi.tapfirst.main.activity.Tutorial;
import faris.tbi.tapfirst.myUtilities.MyUtil;
import faris.tbi.tapfirst.play.activity.PlayActivity;

public class SplashActivity extends AppCompatActivity {


   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_splash);
      Log.e("Splash", "splash " + PlayActivity.TYPE_SCREEN);
      MyUtil.stopMediaCopy();
      if (PlayActivity.SKIP_SCREEN) {
         PlayActivity.SKIP_SCREEN = false;
         startActivity(new Intent(SplashActivity.this, Tutorial.class));
         finish();
      } else {
         if (!PlayActivity.TYPE_SCREEN)
            c.start();
         else {
            PlayActivity.TYPE_SCREEN = false;
            startActivity(new Intent(SplashActivity.this, MainActivity.class).putExtra(PlayActivity.TYPE_MESSAGE, PlayActivity.LOOSE_MSG));
            //PlayActivity.LOOSE_MSG = null;
            finish();
         }
      }

   }


   CountDownTimer c = new CountDownTimer(5000, 5000) {

      public void onTick(long millisUntilFinished) {

      }

      public void onFinish() {
         startActivity(new Intent(SplashActivity.this, Tutorial.class));
         finish();
      }
   };

   @Override
   public void onBackPressed() {
      super.onBackPressed();
      PlayActivity.TYPE_SCREEN = true;
   }
}
