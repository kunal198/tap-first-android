package faris.tbi.tapfirst.main.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import faris.tbi.tapfirst.R;
import faris.tbi.tapfirst.listeners.IonUpdateUI;
import faris.tbi.tapfirst.main.fragment.PlayFragment;
import faris.tbi.tapfirst.main.fragment.PlayersFragment;
import faris.tbi.tapfirst.myUtilities.MyDialogs;
import faris.tbi.tapfirst.myUtilities.MySharedPreference;
import faris.tbi.tapfirst.myUtilities.MyUtil;
import faris.tbi.tapfirst.play.activity.PlayActivity;

public class MainActivity extends AppCompatActivity implements IonUpdateUI {

   private Context context;
   private MyUtil myUtil = new MyUtil();
   private String checkMsg = null;
   private boolean isOnPause = false, isWin = false, notShowAds = false;
   private MyDialogs myDialogs = new MyDialogs();
   private int count = 0, newCount = 0;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);
      PlayActivity.TYPE_SCREEN = false;
      PlayActivity.SKIP_SCREEN = false;
      //MyUtil.stopMediaCopy();
      context = this;
      count = MySharedPreference.getInstance().getThirdComplete(context);
      Log.e("totalCompleteGame", "totalCompleteGame1 " + PlayActivity.SHOW_QUIT_ADS + " " + count + " " + PlayActivity.HIDE_ADS);

      if (count == 3) {
         MyUtil.completeGame(context, getResources());
         MySharedPreference.getInstance().removeThirdComplete(context);
      }

      myUtil.switchfragment(context, new PlayFragment());
      if (getIntent().getExtras() != null) {
         checkMsg = getIntent().getExtras().getString(PlayActivity.TYPE_MESSAGE);
         Log.e("OnCreate", "OnCreate " + checkMsg);
         if (checkMsg != null) {
            newCount++;
            if (checkMsg.equals(PlayActivity.LOOSE_MSG)) {
               MyUtil.playMedia(context, 2, false);
               isWin = false;
            } else {
               MyUtil.playMedia(context, 5, false);
               isWin = true;
            }
            myDialogs.winLossDialog(this, this, isWin);
         } else {
            notShowAds = true;
            new Handler().postDelayed(new Runnable() {
               @Override
               public void run() {
                  MyUtil.playMedia(context, 0, true);
               }
            }, 1 * 1000);
         }
      } else
         new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               MyUtil.playMedia(context, 0, true);
            }
         }, 1 * 1000);

      if (PlayActivity.SHOW_QUIT_ADS && !notShowAds)
         MyUtil.initializeQuitAds(context, getResources(), true);

      PlayActivity.SHOW_QUIT_ADS = false;

   }

   @Override
   protected void onResume() {
      super.onResume();
      MyUtil.stopMediaCopy();
      newCount++;
      checkMsg = null;
      if (isOnPause)
         MyUtil.startMedia();
   }

   @Override
   protected void onPause() {
      super.onPause();
      checkMsg = null;
      isOnPause = true;
      if (newCount == 3)
         myDialogs.dismissDialog();
      MyUtil.pauseMedia();

   }

   @Override
   protected void onDestroy() {
      super.onDestroy();
      checkMsg = null;
   }

   @Override
   public void onSuccess(boolean isReq) {
      MyUtil.stopMediaCopy();
      MyUtil.stopMedia();
      MyUtil.playMedia(context, 0, true);
   }

   @Override
   public void onFail() {

   }

   @Override
   public void onBackPressed() {
      super.onBackPressed();
      PlayActivity.TYPE_SCREEN = false;
      checkMsg = null;
      //finish();
   }

   /* @Override
    public void onDataPass(String data) {
        Log.e("onDataPass", "onDataPass " + data);
    }*/
}
