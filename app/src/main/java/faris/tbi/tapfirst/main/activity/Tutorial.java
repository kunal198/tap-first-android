package faris.tbi.tapfirst.main.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import faris.tbi.tapfirst.R;
import faris.tbi.tapfirst.SplashActivity;
import faris.tbi.tapfirst.myUtilities.MySharedPreference;

public class Tutorial extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tutorial);

        if (MySharedPreference.getInstance().getOneTime(this) != null) {
            startActivity(new Intent(Tutorial.this, MainActivity.class));
            finish();
        }

        ((ImageView) findViewById(R.id.iv_dummy_skip)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MySharedPreference.getInstance().oneTimeTutorial(Tutorial.this);
                startActivity(new Intent(Tutorial.this, MainActivity.class));
                finish();
            }
        });
    }
}
