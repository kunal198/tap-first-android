package faris.tbi.tapfirst.main.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.HashMap;

import faris.tbi.tapfirst.R;
import faris.tbi.tapfirst.myUtilities.MyConstant;
import faris.tbi.tapfirst.myUtilities.views.RobotoTextView;

/**
 * Created by brst-pc81 on 12/26/16.
 */

public class PlayerInfoAdapter extends BaseAdapter {

    private ArrayList<HashMap<String, String>> list;
    private PlayerListAdapter.MyClickListener myClickListener;

    public PlayerInfoAdapter(ArrayList<HashMap<String, String>> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        View view = convertView;
        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_player_item, parent, false);
            holder = new ViewHolder();
            holder.tvPlayer = (RobotoTextView) view.findViewById(R.id.txtv_player);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.tvPlayer.setText(list.get(position).get(MyConstant.NAME));
        return view;
    }

    static class ViewHolder {
        RobotoTextView tvPlayer;
    }

}
