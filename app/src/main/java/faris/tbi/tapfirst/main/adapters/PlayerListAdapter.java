package faris.tbi.tapfirst.main.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import faris.tbi.tapfirst.R;
import faris.tbi.tapfirst.myUtilities.MyConstant;

/**
 * Created by brst-pc93 on 12/13/16.
 */

public class PlayerListAdapter extends RecyclerView.Adapter<PlayerListAdapter.ViewHolder> {

    private ArrayList<HashMap<String, String>> list;
    private MyClickListener myClickListener;


    public class ViewHolder extends RecyclerView.ViewHolder //implements View.OnClickListener
    {

        TextView txtv_player;

        public ViewHolder(View itemView) {

            super(itemView);

            txtv_player = (TextView) itemView.findViewById(R.id.txtv_player);

            //  itemView.setOnClickListener(this);
        }

       /* @Override
        public void onClick(View v)
        {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }*/
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {

        this.myClickListener = myClickListener;
    }

    public PlayerListAdapter(ArrayList<HashMap<String, String>> list) {

        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_player_item, parent, false);

        ViewHolder dataObjectHolder = new ViewHolder(view);

        return dataObjectHolder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtv_player.setText(list.get(position).get(MyConstant.NAME));
    }

    public void addItem(HashMap<String, String> dataObj, int index) {

        list.add(index, dataObj);

        notifyItemInserted(index);
    }

    public void deleteItem(int index) {

        list.remove(index);
        notifyItemRemoved(index);

    }

    @Override
    public int getItemCount() {

        return list.size();
    }

    public interface MyClickListener {

        void onItemClick(int position, View v);
    }
}