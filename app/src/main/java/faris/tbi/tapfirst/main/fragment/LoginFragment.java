package faris.tbi.tapfirst.main.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.Random;

import faris.tbi.tapfirst.R;
import faris.tbi.tapfirst.listeners.IonUpdateUI;
import faris.tbi.tapfirst.myUtilities.Firebase;
import faris.tbi.tapfirst.myUtilities.MySharedPreference;
import faris.tbi.tapfirst.myUtilities.MyUtil;

public class LoginFragment extends Fragment implements View.OnClickListener, IonUpdateUI {

    private EditText edtv_username;
    private View view = null;
    private Context context;
    private MyUtil myUtil;
    private LinearLayout viewMain;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        myUtil = new MyUtil();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = getActivity();

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_login, container, false);
            setUpIds(view);
        }

        return view;
    }

    private void setUpIds(View view) {
        edtv_username = (EditText) view.findViewById(R.id.edtv_username);
        viewMain = (LinearLayout) view.findViewById(R.id.viewMain);
        edtv_username.setHint(getResources().getString(R.string.please_enter_name).toUpperCase());
        view.findViewById(R.id.btn_signUp_dummy_select).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_signUp_dummy_select:
                check();
                break;
        }
    }

    private void check() {
        String userName = edtv_username.getText().toString().trim();

        if (userName.isEmpty())
            MyUtil.showSnackBar(view, "Please enter your name.");
        else {
            //showDialog();
            //MySharedPreference.getInstance().saveUser(context, edtv_username.getText().toString().trim(), "");
            //myUtil.switchfragment(context, new PlayersFragment());
            Firebase.getInstance().createUser(getActivity(), edtv_username.getText().toString().trim(), this);
            try {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(viewMain.getWindowToken(), 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onSuccess(boolean isReq) {
        //MyUtil.showToast(context, "SignUp Successfully");
        myUtil.switchfragment(context, new PlayersFragment());
    }

    @Override
    public void onFail() {
        MyUtil.showToast(context, "Error! Please try again.");
    }
}
