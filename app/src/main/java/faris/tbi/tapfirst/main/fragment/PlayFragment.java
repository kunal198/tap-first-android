package faris.tbi.tapfirst.main.fragment;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import faris.tbi.tapfirst.R;
import faris.tbi.tapfirst.myUtilities.MySharedPreference;
import faris.tbi.tapfirst.myUtilities.MyUtil;
import faris.tbi.tapfirst.play.activity.PlayActivity;

public class PlayFragment extends Fragment implements View.OnClickListener {

    private View view = null;
    private MyUtil myUtil = new MyUtil();
    private MediaPlayer mediaPlayer;
    private int count = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_play, container, false);
            setUpIds(view);
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setUpIds(View view) {
        view.findViewById(R.id.iv_play_dummy_select).setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_play_dummy_select:
                count = MySharedPreference.getInstance().getThirdPresPlay(getActivity());
                count++;
                Log.e("totalCompleteGame", "totalCompleteGameP " + PlayActivity.SHOW_QUIT_ADS + " " + count);
                MySharedPreference.getInstance().setThirdPressPlay(getActivity(), count);
                if (MySharedPreference.getInstance().getUSER_NAME(getActivity()).isEmpty()) {
                    myUtil.switchfragment(PlayFragment.this, new LoginFragment());
                } else {

                    myUtil.switchfragment(PlayFragment.this, new PlayersFragment());
                }

                break;
        }
    }
}


