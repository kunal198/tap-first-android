package faris.tbi.tapfirst.main.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import faris.tbi.tapfirst.R;
import faris.tbi.tapfirst.myUtilities.Firebase;
import faris.tbi.tapfirst.myUtilities.MyConstant;
import faris.tbi.tapfirst.myUtilities.MyDialogs;
import faris.tbi.tapfirst.myUtilities.MySharedPreference;
import faris.tbi.tapfirst.myUtilities.MyUtil;
import faris.tbi.tapfirst.play.activity.ReportActivity;


public class PlayersFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {

   private Spinner spinner;
   private View view = null;
   private Context context;
   private MyUtil myUtil = new MyUtil();
   private MyDialogs myDialogs = new MyDialogs();
   private String[] playerArray = new String[]{"Select Player", "1", "2", "3", "4", "5"};
   private int count = 0;

   @Override
   public void onActivityCreated(Bundle savedInstanceState) {
      super.onActivityCreated(savedInstanceState);
      count = MySharedPreference.getInstance().getThirdPresPlay(getActivity());

      if (count == 3) {
         MyUtil.initializeQuitAds(getActivity(), getResources(), false);
         MySharedPreference.getInstance().removeThirdPressPlay(getActivity());
      } else if (count > 3) {
         MySharedPreference.getInstance().removeThirdPressPlay(getActivity());
      }

      List<String> playerList = new ArrayList<>(Arrays.asList(playerArray));

      // Initializing an ArrayAdapter
      ArrayAdapter<String> playerAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, playerList) {
         @Override
         public boolean isEnabled(int position) {
            if (position == 0)
               return false;
            return true;
         }

         @Override
         public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View view = super.getDropDownView(position, convertView, parent);
            TextView tv = (TextView) view;
            tv.setTextColor(getResources().getColor(R.color.spinner_text_color));
            return view;
         }
      };

      playerAdapter.setDropDownViewResource(R.layout.spinner_item);
      spinner.setAdapter(playerAdapter);
      spinner.setOnItemSelectedListener(this);

   }

   @Override
   public void onResume() {
      super.onResume();
   }

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      context = getActivity().getApplicationContext();
      if (view == null) {
         view = inflater.inflate(R.layout.fragment_players, container, false);
         setUpIds(view);
      }

      return view;
   }

   private void setUpIds(View view) {
      spinner = (Spinner) view.findViewById(R.id.spinner);
      view.findViewById(R.id.iv_dummy_report).setOnClickListener(this);
      view.findViewById(R.id.btn_proceed).setOnClickListener(this);
      view.findViewById(R.id.btn_records).setOnClickListener(this);
   }

   @Override
   public void onClick(View view) {
      switch (view.getId()) {
         case R.id.btn_proceed:
            // myUtil.switchfragment(PlayersFragment.this, new CountDownFragment());
            int pc = Integer.parseInt(spinner.getSelectedItem().toString()) + 1;
            Log.e("pc", "pcpc " + pc);
            //myDialogs.countDownDialod(getActivity(), pc);
            break;

         case R.id.iv_dummy_report:
            //MyUtil.mediaPlayer = null;
            //startActivity(new Intent(getActivity(), ReportActivity.class));
            // myUtil.switchfragment(context, new ReportActivity());
            myUtil.switchfragment(PlayersFragment.this, new ReportActivity());
            break;

         case R.id.btn_records:
            //Firebase.getInstance().checkTypeUsersGroup(getActivity(), MyConstant.TYPE_USERS[0]);
            //myUtil.switchfragment(PlayersFragment.this, new RecordsFragment());
            break;
      }
   }


   @Override
   public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
      TextView tv = (TextView) view;
      if (position == 0)
         tv.setText("Select Player");
      else if (position > 1)
         tv.setText(playerArray[position] + " Players");
      else
         tv.setText("1 Player");

      if (position > 0) {
         Firebase.getInstance().checkTypeUsersGroup(getActivity(), MyConstant.TYPE_USERS[position], PlayersFragment.this);
         Log.e("count", "count " + MyConstant.TYPE_USERS[position]);
            /*Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    myDialogs.countDownDialod(getActivity(), 2);
                }
            }, 1 * 1000);*/

         //myDialogs.countDownDialod(getActivity(), pc);

      }

   }

   @Override
   public void onNothingSelected(AdapterView<?> parent) {

   }

   @Override
   public void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);
      if (requestCode == MyConstant.UPDATE_GAME && resultCode == Activity.RESULT_OK) {
         Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container_body);
         if (fragment != null && fragment instanceof PlayersFragment)
            getActivity().getSupportFragmentManager().popBackStackImmediate();
      }
   }
}