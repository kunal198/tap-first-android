package faris.tbi.tapfirst.main.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import faris.tbi.tapfirst.R;
import faris.tbi.tapfirst.myUtilities.MyUtil;

/**
 * Created by brst-pc93 on 12/13/16.
 */

public class RecordsFragment extends Fragment /*implements View.OnClickListener*/ {

    View view = null;

    MyUtil myUtil = new MyUtil();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_records, container, false);


            // setUpIds(view);
        }


        return view;
    }

  /*  private void setUpIds(View view)
    {

        view.findViewById(R.id.btn_play).setOnClickListener(this);
    }


    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btn_play:

               // myUtil.switchfragment(PlayFragment.this, new LoginFragment());

                break;
        }
    }*/
}