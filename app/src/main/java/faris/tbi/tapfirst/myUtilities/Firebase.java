package faris.tbi.tapfirst.myUtilities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import faris.tbi.tapfirst.listeners.IonUpdateUI;
import faris.tbi.tapfirst.main.activity.MainActivity;
import faris.tbi.tapfirst.play.activity.PlayActivity;

/**
 * Created by brst-pc93 on 10/21/16.
 */

public class Firebase {

   private String TAG = "Firebase";
   public static Firebase instance = null;
   DatabaseReference root = FirebaseDatabase.getInstance().getReference().getRoot();
   private ProgressDialog dialog;
   private int count = 2;
   private String typeUsersGroup = MyConstant.TWO_USERS;
   private Fragment fragment;

   /**
    * singleton initilaization of firebase
    *
    * @return
    */
   public static Firebase getInstance() {
      if (instance == null) {
         instance = new Firebase();
      }

      return instance;
   }

   /**
    * to register player in firebase
    *
    * @param context
    * @param username
    * @param ionUpdateUI
    */

   public void createUser(final Context context, final String username, final IonUpdateUI ionUpdateUI) {
      showDialog(context, MyConstant.REGISTER_MESSAGE);
      HashMap<String, Object> result = new HashMap<>();
      result.put(MyConstant.NAME, username);
      result.put(MyConstant.FCM_TOKEN, MySharedPreference.getInstance().getFCM_TOKEN(context));

      root.child(MyConstant.USERS).push().updateChildren(result, new DatabaseReference.CompletionListener() {
         @Override
         public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
            if (dialog != null)
               dialog.dismiss();

            if (databaseError != null) {
               Log.e("Error ", "" + databaseError.getMessage());
               if (ionUpdateUI != null)
                  ionUpdateUI.onFail();
            } else {
               MySharedPreference.getInstance().saveUser(context, username, databaseReference.getKey());
               if (ionUpdateUI != null)
                  ionUpdateUI.onSuccess(true);
            }

         }
      });

   }

   public void showDialog(Context context, String message) {
      dialog = new ProgressDialog(context);
      dialog.setMessage(message);
      dialog.setCancelable(false);
      dialog.show();
   }

   /**
    * get type of group selected where player wants to play
    *
    * @return
    */
   public DatabaseReference get2UG() {
      Log.e(TAG, TAG + " " + typeUsersGroup);
      return root.child(MyConstant.GROUPS).child(typeUsersGroup);
   }

   /**
    * check which group are selected by players/users
    *
    * @param context
    * @param typeUsers
    * @param fragment
    */

   public void checkTypeUsersGroup(final Context context, String typeUsers, Fragment fragment) {
      final String userId = MySharedPreference.getInstance().getUSER_ID(context);
      typeUsersGroup = typeUsers;
      this.fragment = fragment;

      switch (typeUsers) {
         case MyConstant.TWO_USERS:
            count = 2;
            break;

         case MyConstant.THREE_USERS:
            count = 3;
            break;

         case MyConstant.FOUR_USERS:
            count = 4;
            break;

         case MyConstant.FIVE_USERS:
            count = 5;
            break;

         case MyConstant.SIX_USERS:
            count = 6;
            break;
      }

      Log.e("count", "count " + count);

      get2UG().addListenerForSingleValueEvent(new ValueEventListener() {
             @Override
             public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e("TAGTAG", "CheckGroup " + dataSnapshot.getKey());

                if (dataSnapshot.getValue() == null) {
                   createTypeUsersGroup(context);
                } else {
                   boolean isPartnerAvailable = false;


                   for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                      String key = messageSnapshot.getKey();

                      if (dataSnapshot.child(key).child(MyConstant.USER_COUNT).getValue() != null) {

                         //Log.e("OtherUserID", "" + messageSnapshot.getKey());
                         if ((Long) dataSnapshot.child(key).child(MyConstant.USER_COUNT).getValue() < count) {
                            boolean isThatMyID = false;
                            for (DataSnapshot messageSnapshot2 : dataSnapshot.child(key).child(MyConstant.USERS).getChildren()) {

                               if (userId.equals(messageSnapshot2.getKey())) {
                                  Log.e("OtherUserID", " " + messageSnapshot.getKey());//getting wrong key sometimes
                                  isThatMyID = true;
                               }
                            }

                            if (!isThatMyID) {
                               isPartnerAvailable = true;
                               addInUsersGroup(context, key);
                               break;
                            }
                         }
                      }
                      // Log.e("Name",""+dataSnapshot.child(name).getChildrenCount());
                   }

                   if (!isPartnerAvailable) {
                      createTypeUsersGroup(context);
                   }
                }

                // Log.e("Task",""+dataSnapshot.getValue());
             }

             @Override
             public void onCancelled(DatabaseError databaseError) {
             }
          }

      );
   }

   /**
    * add other player in same group if its space in group
    *
    * @param context
    * @param key
    */

   public void addInUsersGroup(final Context context, final String key) {

      Log.e("Tag", "TAGTAG " + key);

      get2UG().child(key).child(MyConstant.USER_COUNT).runTransaction(new Transaction.Handler() {
         @Override
         public Transaction.Result doTransaction(final MutableData currentData) {
            if (currentData.getValue() == null) {
               currentData.setValue(1);
            } else {
               currentData.setValue((Long) currentData.getValue() + 1);
            }

            return Transaction.success(currentData);
         }

         @Override
         public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
            if (databaseError != null) {
               Log.e("Tag", "Firebase Addition increment failed." + databaseError.getMessage());
               createTypeUsersGroup(context);
            } else {
               Log.e("Tag", "succeeded." + dataSnapshot.getKey());
               Log.e("Tag", "TAGTAG" + key);
               String userId = MySharedPreference.getInstance().getUSER_ID(context);
               String userName = MySharedPreference.getInstance().getUSER_NAME(context);

               HashMap<String, Object> result = new HashMap<>();
               result.put(userId, userName + " : 0 " + MyConstant.PLAYER_STATUS + " " + MyConstant.ONLINE);

               get2UG().child(key).child(MyConstant.USERS).updateChildren(result);
               saveKeyAndPlay(context, key);
            }
         }
      });

   }

   /**
    * create group
    *
    * @param context
    */

   public void createTypeUsersGroup(final Context context) {
      HashMap<String, Object> result = new HashMap<>();
      result.put(MyConstant.USER_COUNT, 1);

      final String key = get2UG().push().getKey();

      Log.e("Tag", "TAGTAG " + key);

      get2UG().child(key).updateChildren(result).addOnCompleteListener(new OnCompleteListener<Void>() {
         @Override
         public void onComplete(@NonNull Task<Void> task) {
            if (task.isSuccessful()) {

               String userId = MySharedPreference.getInstance().getUSER_ID(context);
               String userName = MySharedPreference.getInstance().getUSER_NAME(context);

               HashMap<String, Object> result = new HashMap<>();
               result.put(userId, userName + " : 0 " + MyConstant.PLAYER_STATUS + " " + MyConstant.ONLINE);
               get2UG().child(key).child(MyConstant.USERS).updateChildren(result);
               saveKeyAndPlay(context, key);

            }
         }
      });
   }

   /**
    * save room and move to play screen
    *
    * @param context
    * @param key
    */
   public void saveKeyAndPlay(Context context, String key) {
      MySharedPreference.getInstance().saveRoomKey(context, key);
      Intent intent = new Intent(context, PlayActivity.class);
      intent.putExtra(MyConstant.TYPE_GROUP, typeUsersGroup);
      //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      //fragment.startActivityForResult(intent, MyConstant.UPDATE_GAME);
      context.startActivity(intent);
      MyUtil.stopMedia();
      ((MainActivity) context).finish();
      //Variable on which all the game is played
      setEmptyPoint(context);
   }

   /**
    * get all info of users while participating in the game
    *
    * @param context
    * @return
    */
   public Query getUsers(Context context) {
      return get2UG().child(MySharedPreference.getInstance().getRoomKey(context)).child(MyConstant.USERS);
   }

   public Query getPoints(Context context) {
      return get2UG().child(MySharedPreference.getInstance().getRoomKey(context)).child(MyConstant.POINTS);
   }

   public Task<Void> setEmptyPoint(Context context) {
      return get2UG().child(MySharedPreference.getInstance().getRoomKey(context)).child(MyConstant.POINTS).
         setValue(MyConstant.EMPTY_KEY);
   }

   public Task<Void> setGroupId(Context context, String id) {
      return get2UG().child(MySharedPreference.getInstance().getRoomKey(context)).child(MyConstant.GROUP_ID).
         setValue(id);
   }

   public Query getGroupId(Context context) {
      return get2UG().child(MySharedPreference.getInstance().getRoomKey(context)).child(MyConstant.GROUP_ID);
   }

   public Task<Void> bidForPoint(Context context) {
      return get2UG().child(MySharedPreference.getInstance().getRoomKey(context)).child(MyConstant.POINTS).
         setValue(MySharedPreference.getInstance().getUSER_ID(context));
   }

   /**
    * set score and staus of player
    *
    * @param context
    * @param point
    * @param status
    * @return
    */
   public Task<Void> setPoint(Context context, int point, String status) {
      String userId = MySharedPreference.getInstance().getUSER_ID(context);
      String userName = MySharedPreference.getInstance().getUSER_NAME(context);
      HashMap<String, Object> map = new HashMap<>();
      map.put(userId, userName + " : " + point + " " + MyConstant.PLAYER_STATUS + " " + status);
      return get2UG().child(MySharedPreference.getInstance().getRoomKey(context)).child(MyConstant.USERS).updateChildren(map);
   }

   /**
    * remove user from group if game is not started or if he quit game
    *
    * @param context
    * @param userId
    * @return
    */
   public Task<Void> removeUser(Context context, String userId) {
      return get2UG().child(MySharedPreference.getInstance().getRoomKey(context)).child(MyConstant.USERS).child(userId).setValue(null);
      //return get2UG().child(MySharedPreference.getInstance().getRoomKey(context)).child(MyConstant.USER_COUNT).setValue(l);
   }

   /**
    * remove user count according if user enters and left without game is started
    *
    * @param context
    * @param l
    * @return
    */

   public Task<Void> decreaseCount(Context context, long l) {
      if (l < 0)
         return get2UG().child(MySharedPreference.getInstance().getRoomKey(context)).child(MyConstant.USER_COUNT).setValue(0);
      return get2UG().child(MySharedPreference.getInstance().getRoomKey(context)).child(MyConstant.USER_COUNT).setValue(l);
   }

   /**
    * remove group once the game is initiated and completely over
    *
    * @param context
    * @return
    */
   public Task<Void> removeGroup(Context context) {
      Log.e("groupCheck", "groupCheckIdR " + MySharedPreference.getInstance().getRoomKey(context));
      return get2UG().child(MySharedPreference.getInstance().getRoomKey(context)).removeValue();
   }

// ref.child("firebase-test").orderByChild("title").equalTo("Apple");


/*    public void checkTwoUsersGroup(final Context context)
    {


        String userId = MySharedPreference.getInstance().getUSER_ID(context);
        String userName = MySharedPreference.getInstance().getUSER_NAME(context);

        HashMap<String, Object> result = new HashMap<>();
        result.put(userId, userName);


        root.child(MyConstant.GROUPS).child(MyConstant.TWO_USERS).


    }*/


//
//
//    public Query getMyGroups(Context context)
//    {
//        return root.child(MyConstant.GROUP_CHAT_NAMES).orderByChild(MyConstant.ADMIN_ID).equalTo(MySharedPreference.getInstance().getUSER_ID(context));
//    }
//
//
//    public Query getMySubscribeGroupsIDS(Context context)
//    {
////       return root.child(SUBSCRIBERS).orderByChild(USER_ID).equalTo(MySharedPreference.getInstance().getUSER_ID(context));
////        return root.child(SUBSCRIBERS).ord  public Query getUsers()

//        Log.e("UserID", "" + MySharedPreference.getInstance().getUSER_ID(context));
//        return root.child(MyConstant.SUBSCRIBERS);
//    }
//
//    public Query getMySubscribeGroupsDATA(String groupID)
//    {
//        return root.child(MyConstant.GROUP_CHAT_NAMES).orderByChild(MyConstant.GROUP_ID).equalTo(groupID);
//    }
//

//
//    public Query getUserFcmToken(String userID)
//    {
//        return root.child(MyConstant.USERS).child(userID);
//    }
//
//    public void subscribeUserToGroup(String userID, String userName, String groupID)
//    {
//        HashMap<String, Object> result = new HashMap<>();
//        result.put(userID, userName);
//
//        root.child(MyConstant.SUBSCRIBERS).child(groupID).updateChildren(result);
//    }
//
//
//    public Query getGroupSubscribedUsers(String groupID)
//    {
//       return root.child(MyConstant.SUBSCRIBERS).child(groupID);
//    }


/*    public void updateToken(Context context,String userID)
    {
        HashMap<String, Object> result = new HashMap<>();
        result.put( userID,MySharedPreference.getInstance().getFCM_TOKEN(context));

        root.child(MyConstant.FCM_TOKEN).updateChildren(result).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                Log.e("Exception",""+e);
            }
        });

    }*/

/*
    public void sendNotification(String fcmID, HashMap<String, String> map)
    {
        // String TO = "d87QHGv1CbU:APA91bGg1A0ntLpqcmQhlXU4RckCe9sSIqfmwdiHxLbkenSTVJc-gZMHdT6YVMQxDroUQXKNQOr04QbjUhqI3UtLfmp-svZqPPS-aRt7rewiGDDotcYlByOF6TUOzOfOLX6s99477KZb";

        String body123 = "{\n\t\"to\": \" " + fcmID + "\",\n\t\"notification\" :" + convert(map) + "\n}";

        Log.e("DataToSend", "" + body123);

        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");
        // RequestBody body = RequestBody.create(mediaType, "{\n\t\"to\": \"d87QHGv1CbU:APA91bGg1A0ntLpqcmQhlXU4RckCe9sSIqfmwdiHxLbkenSTVJc-gZMHdT6YVMQxDroUQXKNQOr04QbjUhqI3UtLfmp-svZqPPS-aRt7rewiGDDotcYlByOF6TUOzOfOLX6s99477KZb\",\n\t\"notification\" :{\n\t\t\"body\":\"Testing Balli Daku Navjot\",\n\t\t\"title\":\"Title\"\n\t}\n}");

        RequestBody body = RequestBody.create(mediaType, body123);
        Request request = new Request.Builder()
                .url("https://fcm.googleapis.com/fcm/send")
                .post(body)
                .addHeader("authorization", "key=" + MyConstant.SERVER_KEY)
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")
                //.addHeader("postman-token", "dfe29a6f-e40a-6b27-4065-6a25b103db56")
                .build();


        // Response response = client.newCall(request).execute();


        client.newCall(request).enqueue(new Callback()
        {
            @Override
            public void onFailure(Request request, IOException e)
            {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Response response) throws IOException
            {
                if (!response.isSuccessful())
                {
                    throw new IOException("Unexpected code " + response);
                }
                else
                {
                    Log.e("ResponseBALLI", response.body().string());
                }
            }


        });


    }

    public String convert(HashMap<String, String> map)
    {
        JSONObject obj = null;
        try
        {
            obj = new JSONObject(map);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return obj.toString();
    }*/


}
