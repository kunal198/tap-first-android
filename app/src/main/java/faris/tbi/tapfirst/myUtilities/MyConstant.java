package faris.tbi.tapfirst.myUtilities;

import faris.tbi.tapfirst.R;


/**
 * Created by brst-pc93 on 12/13/16.
 */

public class MyConstant {

    public static boolean IS_MEDIA_CHANGED = false;
    public static final String GROUP_ID = "group_id";

    public static final String NAME = "name";
    public static final String O_NAME = "o_name";
    public static final String POINTS = "points";
    public static final String USER_ID = "user_id";
    public static final String TYPE_GROUP = "type_group";

    public static final String WIN_GAME = "win_game";
    public static final String LOOSE_GAME = "loose_game";
    public static final String TOTAL_GAMES = "total_games";

    public static final String FCM_TOKEN = "fcm_token";
    public static final String USERS = "users";
    public static final String PLAYER_STATUS = "status";
    public static final String ONLINE = "online";
    public static final String OFFLINE = "offline";

    public static final String GROUPS = "groups";
    public static final String USER_COUNT = "user_count";
    public static int UPDATE_GAME = 12;

    public static final String NO_USERS = "no_users";
    public static final String TWO_USERS = "two_users";
    public static final String THREE_USERS = "three_users";
    public static final String FOUR_USERS = "four_users";
    public static final String FIVE_USERS = "five_users";
    public static final String SIX_USERS = "six_users";


    public static final String ROOM_KEY = "room_key";
    public static final String EMPTY_KEY = "Empty";
    public static final String REGISTER_MESSAGE = "Please wait...";
    public static final String[] TYPE_USERS = {NO_USERS, TWO_USERS, THREE_USERS, FOUR_USERS, FIVE_USERS, SIX_USERS};

    public static final int ALL_SCREEN = R.raw.all_screen;
    public static final int GAME_PLAY = R.raw.game_play;
    public static final int LOSE = R.raw.lose;
    public static final int PLAY_1_2_3 = R.raw.play_1_2_3;
    public static final int TAP_CLICK = R.raw.tap_click;
    public static final int WIN = R.raw.win;

    public static final int[] TYPE_MUSIC = {ALL_SCREEN, GAME_PLAY, LOSE, PLAY_1_2_3, TAP_CLICK, WIN};


}
