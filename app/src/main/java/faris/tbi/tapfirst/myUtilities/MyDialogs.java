package faris.tbi.tapfirst.myUtilities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import faris.tbi.tapfirst.R;
import faris.tbi.tapfirst.listeners.IonUpdateUI;
import faris.tbi.tapfirst.myUtilities.views.RobotoTextView;
import faris.tbi.tapfirst.play.activity.PlayActivity;

/**
 * Created by brst-pc93 on 12/13/16.
 */

public class MyDialogs implements View.OnClickListener {

   private int count = 0;
   private Dialog dialog;
   private TextView txtv_timer;
   private IonUpdateUI ionUpdateUI;
   private RobotoTextView tvWinLossMsg;
   private ImageButton ibClose;
   private ImageView ivLoosHeart, ivWinOk, ivLossRetry, ivDummy;
   private boolean isExit = false;

   public void countDownDialod(Context context, IonUpdateUI ionUpdateUI) {
      this.ionUpdateUI = ionUpdateUI;
      dialog = new Dialog(context, R.style.AppTheme);
      dialog.setContentView(R.layout.dialog_countdown);
      txtv_timer = (TextView) dialog.findViewById(R.id.txtv_timer);
      if (!((PlayActivity) context).isFinishing()) {
         dialog.show();
         dialog.setCancelable(false);
         MyCountDownTimer myCountDownTimer = new MyCountDownTimer(context, 3300, 1000);
         myCountDownTimer.start();
      }

   }

   public void winLossDialog(Context context, IonUpdateUI ionUpdateUI, boolean isWin) {
      this.ionUpdateUI = ionUpdateUI;
      dialog = new Dialog(context);
      dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
      dialog.getWindow().setLayout((context.getResources().getDisplayMetrics().widthPixels * 95) / 100,
         WindowManager.LayoutParams.WRAP_CONTENT);
      dialog.setContentView(R.layout.win_loss_popup);
      dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

      tvWinLossMsg = (RobotoTextView) dialog.findViewById(R.id.tv_win_loss_msg);
      ibClose = (ImageButton) dialog.findViewById(R.id.ib_close);
      ivLoosHeart = (ImageView) dialog.findViewById(R.id.iv_heart);
      ivWinOk = (ImageView) dialog.findViewById(R.id.iv_win_ok);
      ivLossRetry = (ImageView) dialog.findViewById(R.id.iv_loss_retry);
      ivDummy = (ImageView) dialog.findViewById(R.id.iv_dummy);

      if (isWin) {
         tvWinLossMsg.setText("You Win");
         ivWinOk.setVisibility(View.VISIBLE);
         ivLoosHeart.setVisibility(View.GONE);
         ivLossRetry.setVisibility(View.GONE);
      } else {
         tvWinLossMsg.setText("You Lost");
         ivLoosHeart.setVisibility(View.VISIBLE);
         ivLossRetry.setVisibility(View.VISIBLE);
         ivWinOk.setVisibility(View.GONE);
      }

      ivDummy.setOnClickListener(this);
      ivWinOk.setOnClickListener(this);
      ivLossRetry.setOnClickListener(this);
      ibClose.setOnClickListener(this);
      dialog.setCancelable(false);
      dialog.show();

   }

   public void cancelDialog() {
      //if (!isExit) {
      if (dialog != null && dialog.isShowing()) {
         dialog.dismiss();

         MyUtil.stopMediaCopy();

         if (ionUpdateUI != null)
            ionUpdateUI.onFail();

      }
      // }
   }

   public void dismissDialog() {
      if (dialog != null && dialog.isShowing()) {
         dialog.dismiss();

         if (ionUpdateUI != null)
            ionUpdateUI.onSuccess(true);
      }
   }

   @Override
   public void onClick(View v) {
      switch (v.getId()) {
         case R.id.iv_loss_retry:
         case R.id.iv_win_ok:
         case R.id.ib_close:
         case R.id.iv_dummy:

            dialog.dismiss();
            if (isExit) {
               if (ionUpdateUI != null)
                  ionUpdateUI.onFail();

            } else {
               if (ionUpdateUI != null)
                  ionUpdateUI.onSuccess(false);
               ionUpdateUI = null;
            }
            break;
      }
   }


   class MyCountDownTimer extends CountDownTimer {
      private Context context;

      public MyCountDownTimer(Context context, long millisInFuture, long countDownInterval) {
         super(millisInFuture, countDownInterval);
         this.context = context;
         count = 4;
         //MyUtil.playMedia(context, 3, false);
         //txtv_timer.setText("" + 3);


      }

      @Override
      public void onTick(long l) {
         Log.e("onTick", "onTick " + count);
         if (!((PlayActivity) context).isFinishing()) {
            MyUtil.playMedia(context, 3, false);
         } else {
            MyUtil.stopMediaCopy();
         }
         --count;
         txtv_timer.setText("" + count);
         dialog.setCancelable(false);
      }

      @Override
      public void onFinish() {
         if (!((PlayActivity) context).isFinishing()) {
            MyUtil.playMedia(context, 3, false);
         } else {
            MyUtil.stopMediaCopy();
         }
         txtv_timer.setTextSize(TypedValue.COMPLEX_UNIT_SP, 40);
         txtv_timer.setText("Play!");
         Handler handler = new Handler();
         handler.postDelayed(new Runnable() {
            @Override
            public void run() {
               c.start();
               if (!((PlayActivity) context).isFinishing())
                  if (ionUpdateUI != null)
                     ionUpdateUI.onSuccess(true);
               //MyUtil.stopMedia();
            }
         }, 1 * 1000);


      }
   }

   CountDownTimer c = new CountDownTimer(200, 200) {

      public void onTick(long millisUntilFinished) {
      }

      public void onFinish() {
         if (dialog != null) {
            dialog.dismiss();
            dialog = null;
         }
      }
   };


}
