package faris.tbi.tapfirst.myUtilities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by brst-pc93 on 10/21/16.
 */

public class MySharedPreference {
   public final String PreferenceName = "MyPreference";
   public final String TUTORIAL = "tutorial_tap";
   public final String THIRD_PRESS_PLAY = "third_press_play";
   public final String THIRD_COMPLETE_GAME = "third_complete_game";

   public MySharedPreference() {
   }

   public static MySharedPreference instance = null;

   public static MySharedPreference getInstance() {
      if (instance == null) {
         instance = new MySharedPreference();
      }
      return instance;
   }

   public SharedPreferences getPreference(Context context) {
      return context.getSharedPreferences(PreferenceName, Activity.MODE_PRIVATE);
   }

   public void oneTimeTutorial(Context context) {
      SharedPreferences.Editor editor = getPreference(context).edit();
      editor.putString(TUTORIAL, "one_time");
      editor.apply();
   }

   public String getOneTime(Context context) {
      return getPreference(context).getString(TUTORIAL, null);
   }

   public void saveUser(Context context, String username, String userid) {
      SharedPreferences.Editor editor = getPreference(context).edit();
      editor.putString(MyConstant.NAME, username);
      editor.putString(MyConstant.USER_ID, userid);
      editor.apply();
   }


   public void saveToken(Context context, String fcmToken) {
      SharedPreferences.Editor editor = getPreference(context).edit();
      editor.putString(MyConstant.FCM_TOKEN, fcmToken);
      editor.apply();
   }

   public void saveTotalGames(Context context, int score) {
      SharedPreferences.Editor editor = getPreference(context).edit();
      editor.putInt(MyConstant.TOTAL_GAMES, score);
      editor.apply();
   }


   public void saveWinGame(Context context, int score) {
      SharedPreferences.Editor editor = getPreference(context).edit();
      editor.putInt(MyConstant.WIN_GAME, score);
      editor.apply();
   }

   /**
    * for third time pressing on play button
    *
    * @param context
    * @param count
    */
   public void setThirdPressPlay(Context context, int count) {
      SharedPreferences.Editor editor = getPreference(context).edit();
      editor.putInt(THIRD_PRESS_PLAY, count);
      editor.apply();
   }

   public int getThirdPresPlay(Context context) {
      return getPreference(context).getInt(THIRD_PRESS_PLAY, 0);
   }

   public void removeThirdPressPlay(Context context) {
      getPreference(context).edit().remove(THIRD_PRESS_PLAY).commit();
   }

   /**
    * for third completeion of full game
    *
    * @param context
    * @param count
    */

   public void setThirdComplete(Context context, int count) {
      SharedPreferences.Editor editor = getPreference(context).edit();
      editor.putInt(THIRD_COMPLETE_GAME, count);
      editor.apply();
   }

   public int getThirdComplete(Context context) {
      return getPreference(context).getInt(THIRD_COMPLETE_GAME, 0);
   }

   public void removeThirdComplete(Context context) {
      Log.e("totalCompleteGame", "totalCompleteGameR ");
      getPreference(context).edit().remove(THIRD_COMPLETE_GAME).commit();
   }


   public void saveLooseGame(Context context, int score) {
      SharedPreferences.Editor editor = getPreference(context).edit();
      editor.putInt(MyConstant.LOOSE_GAME, score);
      editor.apply();
   }

   public int getLooseGames(Context context) {
      return getPreference(context).getInt(MyConstant.LOOSE_GAME, 0);
   }

   public String getUSER_ID(Context context) {
      return getPreference(context).getString(MyConstant.USER_ID, "");
   }

   public int getTotalGames(Context context) {
      return getPreference(context).getInt(MyConstant.TOTAL_GAMES, 0);
   }

   public int getWinGames(Context context) {
      return getPreference(context).getInt(MyConstant.WIN_GAME, 0);
   }

   public String getUSER_NAME(Context context) {
      return getPreference(context).getString(MyConstant.NAME, "");
   }

   public String getFCM_TOKEN(Context context) {
      return getPreference(context).getString(MyConstant.FCM_TOKEN, "");
   }

   //Save Game Room Key
   public void saveRoomKey(Context context, String key) {
      SharedPreferences.Editor editor = getPreference(context).edit();
      editor.putString(MyConstant.ROOM_KEY, key);
      editor.apply();
   }

   public String getRoomKey(Context context) {
      return getPreference(context).getString(MyConstant.ROOM_KEY, "");
   }
//
//
//    //To check current chat Group ID
//    public void saveCurrentGroupID(Context context,String groupID)
//    {
//        SharedPreferences.Editor editor = getPreference(context).edit();
//        editor.putString(MyConstant.GROUP_ID, groupID);
//        editor.apply();
//    }
//
//    public String getCurrentGroupID(Context context)
//    {
//        return getPreference(context).getString(MyConstant.GROUP_ID,"");
//    }
}
