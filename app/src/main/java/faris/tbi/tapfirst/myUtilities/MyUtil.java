package faris.tbi.tapfirst.myUtilities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import faris.tbi.tapfirst.R;

/**
 * Created by brst-pc93 on 12/12/16.
 */

public class MyUtil {

   public static Snackbar snackbar = null;
   public static Toast toast;
   public static ProgressDialog dialog;
   public static MediaPlayer mediaPlayer = null, mp = null;

   public static void showSnackBar(View view, String message) {
      snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
      snackbar.show();
   }


   /**
    * need to extra initialize of mediplayer on play screen to remove instance on playscreen
    *
    * @param context
    * @param position
    * @return
    */
   public static MediaPlayer playMediaCopy(Context context, int position, boolean isPlayingScreen) {
      mp = null;
      mp = MediaPlayer.create(context, MyConstant.TYPE_MUSIC[position]);
      mp.start();
      if (isPlayingScreen)
         mp.setLooping(true);
      return mp;
   }

   /**
    * to stop music
    */
   public static void stopMediaCopy() {
      if (mp != null) {
         Log.e("stopMedia", "stopMedia1 ");
         mp.stop();
         mp.release();
         mp = null;
      }
   }

   /**
    * initialize mediaplayer
    *
    * @param context
    * @param position
    * @param isPlayingScreen
    * @return
    */

   public static MediaPlayer playMedia(Context context, int position, boolean isPlayingScreen) {
      mediaPlayer = null;
      mediaPlayer = MediaPlayer.create(context, MyConstant.TYPE_MUSIC[position]);
      mediaPlayer.start();
      if (isPlayingScreen)
         mediaPlayer.setLooping(true);
      return mediaPlayer;

   }

   /**
    * to stop media
    */
   public static void stopMedia() {
      if (mediaPlayer != null) {
         Log.e("stopMedia", "stopMedia ");
         mediaPlayer.stop();
         mediaPlayer.release();
         mediaPlayer = null;
      }
   }

   /**
    * to pause where actually playing
    */
   public static void pauseMedia() {
      if (mediaPlayer != null)
         mediaPlayer.pause();
   }

   /**
    * to start where actually pause last time
    */

   public static void startMedia() {

      if (mediaPlayer != null && !mediaPlayer.isPlaying())
         mediaPlayer.start();
   }

   /**
    * to show toast
    *
    * @param context
    * @param message
    */
   public static void showToast(Context context, String message) {
      if (toast != null) {
         toast.cancel();
      }
      toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
      toast.show();
   }

   /**
    * to show progress dialog
    *
    * @param context
    * @param message
    * @return
    */
   public static ProgressDialog showDialog(Context context, String message) {
      dialog = new ProgressDialog(context);
      dialog.setMessage(message);
      dialog.setCancelable(false);
      dialog.show();
      return dialog;
   }

   /**
    * for banner initialization
    *
    * @param c
    * @param resources
    */
   public static void initializeAds(Context c, Resources resources) {
      String adId = resources.getString(R.string.ads_play_screen_id);
      MobileAds.initialize(c, adId);
   }

   /**
    * for banner view request
    *
    * @param adView
    */
   public static void admobGoogle(AdView adView) {
      AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
      adView.loadAd(adRequest);
   }

   /**
    * show and display ads everytime user quits the game or every 3 time to press on play button
    *
    * @param resources
    * @param context
    * @return
    */
   public static InterstitialAd initializeQuitAds(Context context, Resources resources, boolean isQuitAds) {
      final InterstitialAd mInterstitialAd = new InterstitialAd(context);
      String adId = null;
      if (isQuitAds)
         adId = resources.getString(R.string.ads_user_quit_id);
      else {
         adId = resources.getString(R.string.ads_every_3rd_press_play_id);
      }
      mInterstitialAd.setAdUnitId(adId);
      AdRequest adRequest = new AdRequest.Builder()
         .build();
      // Load ads into Interstitial Ads
      mInterstitialAd.loadAd(adRequest);
      mInterstitialAd.setAdListener(new AdListener() {
         public void onAdLoaded() {
            if (mInterstitialAd.isLoaded()) {
               mInterstitialAd.show();
            }

         }
      });

      return mInterstitialAd;
   }

   /**
    * show this ads while complete 3 games
    *
    * @param context
    * @param resources
    * @return
    */

   public static InterstitialAd completeGame(Context context, Resources resources) {
      final InterstitialAd mInterstitialAd = new InterstitialAd(context);
      String adId;
      adId = resources.getString(R.string.ads_every_3rd_completed_id);
      mInterstitialAd.setAdUnitId(adId);
      AdRequest adRequest = new AdRequest.Builder()
         .build();
      // Load ads into Interstitial Ads
      mInterstitialAd.loadAd(adRequest);
      mInterstitialAd.setAdListener(new AdListener() {
         public void onAdLoaded() {
            if (mInterstitialAd.isLoaded()) {
               mInterstitialAd.show();
            }

         }
      });
      return mInterstitialAd;
   }

    /*Switch Fragment*/

   public void switchfragment(Fragment fromWhere, Fragment toWhere) {
      FragmentTransaction fragmentTransaction = fromWhere.getActivity().getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.container_body, toWhere);
      fragmentTransaction.addToBackStack(null);
      fragmentTransaction.commit();
   }

   public void switchfragment(Context context, Fragment toWhere) {
      FragmentTransaction fragmentTransaction = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.container_body, toWhere);
      //fragmentTransaction.addToBackStack(null);
      //if (toWhere instanceof PlayersFragment)
      //fragmentTransaction.remove(toWhere);
      fragmentTransaction.commit();

   }


}
