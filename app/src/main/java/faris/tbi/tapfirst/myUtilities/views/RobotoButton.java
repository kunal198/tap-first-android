package faris.tbi.tapfirst.myUtilities.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class RobotoButton extends Button {

	public RobotoButton(Context context) {
		super(context);
	}

	public RobotoButton(Context context, AttributeSet attrs) {
		super(context, attrs);

		if (!isInEditMode()) {
			RobotoTextViewUtils.initTypeface(this, context, attrs);
		}
	}

	public RobotoButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		if (!isInEditMode()) {
			RobotoTextViewUtils.initTypeface(this, context, attrs);
		}
	}

}