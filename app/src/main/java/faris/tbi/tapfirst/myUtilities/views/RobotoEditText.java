package faris.tbi.tapfirst.myUtilities.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class RobotoEditText extends EditText {

	public RobotoEditText(Context context) {
		super(context);
	}

	public RobotoEditText(Context context, AttributeSet attrs) {
		super(context, attrs);

		if (!isInEditMode()) {
			RobotoTextViewUtils.initTypeface(this, context, attrs);
		}
	}

	public RobotoEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		if (!isInEditMode()) {
			RobotoTextViewUtils.initTypeface(this, context, attrs);
		}
	}

	@Override
	public void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
		if (text.length() == 0) {
			setTypeface(getTypeface(), Typeface.ITALIC);
		} else {
			setTypeface(getTypeface(), Typeface.NORMAL);
		}
	}
}
