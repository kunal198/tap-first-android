package faris.tbi.tapfirst.myUtilities.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class RobotoTextView extends TextView {

	public RobotoTextView(Context context) {
		super(context);
	}

	public RobotoTextView(Context context, AttributeSet attrs) {
		super(context, attrs);

		if (!isInEditMode()) {
			RobotoTextViewUtils.initTypeface(this, context, attrs);
		}
	}

	public RobotoTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		if (!isInEditMode()) {
			RobotoTextViewUtils.initTypeface(this, context, attrs);
		}
	}

}