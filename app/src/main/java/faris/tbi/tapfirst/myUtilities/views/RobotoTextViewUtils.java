package faris.tbi.tapfirst.myUtilities.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import faris.tbi.tapfirst.R;


public class RobotoTextViewUtils {

	private RobotoTextViewUtils() {
	}

	public static void initTypeface(TextView textView, Context context, AttributeSet attrs) {
		Typeface typeface = null;

		if (attrs != null) {
			TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RobotoTextView);

			if (a.hasValue(R.styleable.RobotoTextView_typeface)) {
				int typefaceValue = a.getInt(R.styleable.RobotoTextView_typeface, RobotoTypefaceManager.Typeface.ROBOTO_MEDIUM);
				typeface = RobotoTypefaceManager.obtainTypeface(context, typefaceValue);
			}
			a.recycle();
		} else {
			typeface = RobotoTypefaceManager.obtainTypeface(context, RobotoTypefaceManager.Typeface.ROBOTO_MEDIUM);
		}

		setTypeface(textView, typeface);
	}

	public static void initTypeface(EditText textView, Context context, AttributeSet attrs) {
		Typeface typeface = null;

		if (attrs != null) {
			TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RobotoTextView);

			if (a.hasValue(R.styleable.RobotoTextView_typeface)) {
				int typefaceValue = a.getInt(R.styleable.RobotoTextView_typeface, RobotoTypefaceManager.Typeface.ROBOTO_MEDIUM);
				typeface = RobotoTypefaceManager.obtainTypeface(context, typefaceValue);
			}
			a.recycle();
		} else {
			typeface = RobotoTypefaceManager.obtainTypeface(context, RobotoTypefaceManager.Typeface.ROBOTO_MEDIUM);
		}

		setTypeface(textView, typeface);
	}

	public static void initTypeface(Button textView, Context context, AttributeSet attrs) {
		Typeface typeface = null;

		if (attrs != null) {
			TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RobotoTextView);

			if (a.hasValue(R.styleable.RobotoTextView_typeface)) {
				int typefaceValue = a.getInt(R.styleable.RobotoTextView_typeface, RobotoTypefaceManager.Typeface.ROBOTO_MEDIUM);
				typeface = RobotoTypefaceManager.obtainTypeface(context, typefaceValue);
			}
			a.recycle();
		} else {
			typeface = RobotoTypefaceManager.obtainTypeface(context, RobotoTypefaceManager.Typeface.ROBOTO_MEDIUM);
		}

		setTypeface(textView, typeface);
	}

	public static void initTypeface(RadioButton textView, Context context, AttributeSet attrs) {
		Typeface typeface = null;

		if (attrs != null) {
			TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RobotoTextView);

			if (a.hasValue(R.styleable.RobotoTextView_typeface)) {
				int typefaceValue = a.getInt(R.styleable.RobotoTextView_typeface, RobotoTypefaceManager.Typeface.ROBOTO_MEDIUM);
				typeface = RobotoTypefaceManager.obtainTypeface(context, typefaceValue);
			}
			a.recycle();
		} else {
			typeface = RobotoTypefaceManager.obtainTypeface(context, RobotoTypefaceManager.Typeface.ROBOTO_MEDIUM);
		}

		setTypeface(textView, typeface);
	}

	public static void setTypeface(RadioButton textView, Typeface typeface) {
		textView.setPaintFlags(textView.getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
		textView.setTypeface(typeface);
	}

	public static void setTypeface(TextView textView, Typeface typeface) {
		textView.setPaintFlags(textView.getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
		textView.setTypeface(typeface);
	}

	public static void setTypeface(Button textView, Typeface typeface) {
		textView.setPaintFlags(textView.getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
		textView.setTypeface(typeface);
	}

	public static void setTypeface(Paint paint, Typeface typeface) {
		paint.setFlags(paint.getFlags() | Paint.SUBPIXEL_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
		paint.setTypeface(typeface);
	}

}