package faris.tbi.tapfirst.myUtilities.views;

import android.content.Context;
import android.util.SparseArray;

public class RobotoTypefaceManager {
	private final static SparseArray<android.graphics.Typeface> mTypefaces = new SparseArray<android.graphics.Typeface>(22);

	public static android.graphics.Typeface obtainTypeface(Context context, int typefaceValue) throws IllegalArgumentException {
		android.graphics.Typeface typeface = mTypefaces.get(typefaceValue);
		if (typeface == null) {
			typeface = createTypeface(context, typefaceValue);
			mTypefaces.put(typefaceValue, typeface);
		}
		return typeface;
	}

	private static android.graphics.Typeface createTypeface(Context context, int typefaceValue) throws IllegalArgumentException {
		String typefacePath;
		switch (typefaceValue) {

		case Typeface.ROBOTO_MEDIUM:
			typefacePath = "roboto_medium.ttf";
			break;
		default:
			throw new IllegalArgumentException("Unknown `typeface` attribute value " + typefaceValue);
		}

		return android.graphics.Typeface.createFromAsset(context.getAssets(), typefacePath);
	}

	public class Typeface {
		public final static int ROBOTO_MEDIUM = 0;
	}
}