package faris.tbi.tapfirst.play.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import faris.tbi.tapfirst.R;
import faris.tbi.tapfirst.listeners.IonUpdateUI;
import faris.tbi.tapfirst.main.activity.MainActivity;
import faris.tbi.tapfirst.main.adapters.PlayerInfoAdapter;
import faris.tbi.tapfirst.myUtilities.Firebase;
import faris.tbi.tapfirst.myUtilities.MyConstant;
import faris.tbi.tapfirst.myUtilities.MyDialogs;
import faris.tbi.tapfirst.myUtilities.MySharedPreference;
import faris.tbi.tapfirst.myUtilities.MyUtil;

public class PlayActivity extends AppCompatActivity implements IonUpdateUI {

   public static String WIN_MSG = "Congrats! You Won!", LOOSE_MSG = "OOps! Sorry You Lost!";
   public static String TYPE_MESSAGE = "type_of_message";
   public static boolean SKIP_SCREEN = false;
   public static boolean TYPE_SCREEN = false;
   public static boolean SHOW_QUIT_ADS = false;
   public static boolean HIDE_ADS = false;

   private RelativeLayout relativelayout_open_area;
   private ImageButton imageView;
   private GridView mGridView;
   private PlayerInfoAdapter playerInfoAdapter;

   private Context context;
   private String userId;
   private int totalpoints = 0, pos = -1, totalCompleteGame = 0;
   private ArrayList<HashMap<String, String>> list;
   private String score, typeGroup, status = MyConstant.ONLINE;

   private String scoreArr[], nameArr[], userName;
   private boolean isWinGame = false, isQuit = false, isQuit_MyId = false, isMyId = false;
   private boolean isOnPause = true;
   private int[] objects = {R.mipmap.skyblue, R.mipmap.red, R.mipmap.purple, R.mipmap.green,
      R.mipmap.darkblue, R.mipmap.darkgreen, R.mipmap.maroon, R.mipmap.brown};

   private int winGame = 0, playerCount = 0, totalGames = 0, size = 0;
   private ProgressDialog dialog;
   private boolean isOut = false, isCount = false, isGameFinish = false, isStartGame = true, isGameMode = false;
   private boolean isPlay = false;
   private MyDialogs myDialogs = new MyDialogs();

   private int[] timeArr = {2, 5, 1, 6, 7, 3, 8, 4};
   private HashMap<String, String> map;
   private String ownName, typeMsg = null;
   private int h, w;
   private long userCount = 0, mCount = 0, nCount = 0;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_play);
      context = this;
      setUpIds();
      imageView = new ImageButton(PlayActivity.this);
      imageView.setMaxHeight(60);
      imageView.setMaxWidth(60);
      relativelayout_open_area.addView(imageView);

      userId = MySharedPreference.getInstance().getUSER_ID(context);
      dialog = MyUtil.showDialog(this, "Waiting for other players");
      if (getIntent().getExtras() != null) {
         typeGroup = getIntent().getExtras().getString(MyConstant.TYPE_GROUP);
         setListeners();
         getSize();
      }

      MyUtil.initializeAds(context, getResources());
      AdView mAdView = (AdView) findViewById(R.id.adView);
      MyUtil.admobGoogle(mAdView);

      Firebase.getInstance().setGroupId(context, MySharedPreference.getInstance().getRoomKey(context));
      //getListners();
   }

   private void getSize() {

      switch (typeGroup) {

         case MyConstant.TWO_USERS:
            size = 2;
            break;

         case MyConstant.THREE_USERS:
            size = 3;
            break;

         case MyConstant.FOUR_USERS:
            size = 4;
            break;

         case MyConstant.FIVE_USERS:
            size = 5;
            break;

         case MyConstant.SIX_USERS:
            size = 6;
            break;

      }
   }

   @Override
   protected void onResume() {
      super.onResume();
      if (isGameMode) {
         if (isOut) {
            if (!typeGroup.equals(MyConstant.TWO_USERS)) {
               if (mCount != 1)
                  typeMsg = LOOSE_MSG;
               else
                  typeMsg = null;
            } else {
               typeMsg = LOOSE_MSG;
            }
            Log.e("isgmae", "isgmae4");
            //MyUtil.showToast(context, LOOSE_MSG);
            changeScreen();
            isOnPause = false;
            //isGameMode = false;
            //finish();
            if (imageView != null)
               imageView.setClickable(false);

            new Handler().postDelayed(new Runnable() {
               @Override
               public void run() {
                  //changeScreen();
                  finish();
                  if (!typeGroup.equals(MyConstant.TWO_USERS)) {
                     if (!SKIP_SCREEN) {
                        isOnPause = false;
                     }
                  }
               }
            }, 3 * 1000);

            new Handler().postDelayed(new Runnable() {
               @Override
               public void run() {
                  isOnPause = true;
               }
            }, 5 * 1000);

         }
      }
      MyUtil.stopMediaCopy();
      //else
      //Firebase.getInstance().removeUser(context, userId);
   }

   private void stopOnPause() {
      //value = 1;
      isOut = true;
      if (isCount) {
         switch (typeGroup) {
            case MyConstant.TWO_USERS:
               if (isOnPause) {
                  isQuit = true;
                  status = MyConstant.OFFLINE;
                  if (isGameMode) {
                     //SHOW_QUIT_ADS = true;
                     Firebase.getInstance().setPoint(context, totalpoints, status);

                  }
               }
               break;
            case MyConstant.THREE_USERS:
            case MyConstant.FOUR_USERS:
            case MyConstant.FIVE_USERS:
            case MyConstant.SIX_USERS:
               isQuit = true;
               status = MyConstant.OFFLINE;
               //if (pauseCount == 1)
               if (isGameMode)
                  if (isOnPause) {
                     SHOW_QUIT_ADS = true;
                     Firebase.getInstance().setPoint(context, totalpoints, status);
                  }
               //pauseCount = 0;
               break;
         }
      }
   }

   @Override
   protected void onPause() {
      super.onPause();
      myDialogs.cancelDialog();
      if (getIntent().getExtras() != null) {

         if (typeGroup.equals(MyConstant.TWO_USERS))
            if (isGameFinish) {
               if (!isGameMode) {
                  Firebase.getInstance().decreaseCount(context, userCount - 1);
                  Log.e("isgmae", "isgmae2");
                  changeScreen();
                  finish();
               }
               Firebase.getInstance().removeUser(context, userId);
            }
         // if (!typeGroup.equals(MyConstant.TWO_USERS))
         if (!isCount) {
            if (!isGameMode) {
               nCount++;
               Firebase.getInstance().decreaseCount(context, userCount - 1);
               SKIP_SCREEN = true;
               Log.e("isgmae", "isgmae3 " + nCount);
               typeMsg = null;
               changeScreen();
               finish();
            }
            Firebase.getInstance().removeUser(context, userId);

            if (dialog != null)
               dialog.dismiss();
         }
         stopOnPause();
      }
      MyUtil.stopMediaCopy();
   }

   private void callDestroy() {
      //value = 1;
      if (dialog != null)
         dialog.dismiss();

      if (isCount) {
         switch (typeGroup) {

            case MyConstant.TWO_USERS:
               isQuit = true;
               status = MyConstant.OFFLINE;
               Firebase.getInstance().setPoint(context, totalpoints, status);
               break;
            case MyConstant.THREE_USERS:
            case MyConstant.FOUR_USERS:
            case MyConstant.FIVE_USERS:
            case MyConstant.SIX_USERS:
               isQuit = true;
               status = MyConstant.OFFLINE;
               SHOW_QUIT_ADS = true;
               Firebase.getInstance().setPoint(context, totalpoints, status);
               break;
         }
      }

      if (isGameMode) {
         typeMsg = LOOSE_MSG;
         changeScreen();
         finish();
      }
   }

   @Override
   public void onBackPressed() {
      super.onBackPressed();
      isOnPause = false;
      callDestroy();
   }

   private void getListners() {
      Query query = Firebase.getInstance().getGroupId(context);
      query.addValueEventListener(new ValueEventListener() {
         @Override
         public void onDataChange(DataSnapshot dataSnapshot) {
         }

         @Override
         public void onCancelled(DatabaseError databaseError) {

         }
      });
   }

   private void setListeners() {
      Query query = Firebase.getInstance().getUsers(context);
      query.addValueEventListener(new ValueEventListener() {
         @Override
         public void onDataChange(DataSnapshot dataSnapshot) {
            if (typeGroup.equals(MyConstant.TWO_USERS))
               relativelayout_open_area.post(new Runnable() {
                  @Override
                  public void run() {

                     if (isGameMode)
                        imageView.setVisibility(View.GONE);
                  }
               });

            userCount = dataSnapshot.getChildrenCount();
            if (dataSnapshot != null) {

               list = new ArrayList<>();
               Iterator i = dataSnapshot.getChildren().iterator();
               DataSnapshot s = null;
               while (i.hasNext()) {
                  s = (DataSnapshot) i.next();
                  map = new HashMap<>();
                  nameArr = ((String) s.getValue()).split(MyConstant.PLAYER_STATUS);
                  userName = nameArr[0];
                  map.put(MyConstant.USER_ID, s.getKey());
                  map.put(MyConstant.NAME, userName);
                  map.put(MyConstant.O_NAME, (String) s.getValue());
                  list.add(map);
                  if (userId.equals(s.getKey())) ownName = userName;
                  score = s.getValue().toString();
                  scoreArr = score.split(":");
                  scoreArr = scoreArr[1].split(MyConstant.PLAYER_STATUS);

                  //otherName = userName.split(":");
                  //name = otherName[0];

                  if (userId.equals(s.getKey())) isMyId = true;

                  if (typeGroup.equals(MyConstant.TWO_USERS)) {

                     if (nameArr[1].trim().equalsIgnoreCase(MyConstant.OFFLINE)) {
                        isQuit = true;

                        if (userId.equals(s.getKey())) {
                           isQuit_MyId = true;
                           SHOW_QUIT_ADS = true;
                        }
                     }

                  } else {
                     if (nameArr[1].trim().equalsIgnoreCase(MyConstant.OFFLINE) && !isOut) {
                        if (userId.equals(s.getKey())) {
                           isQuit_MyId = true;
                           SHOW_QUIT_ADS = true;
                        }
                        getTypeGroup(s.getKey(), true, nameArr[1]);
                     }
                  }
                  if (s != null && scoreArr[0].trim().toString().equals("10")) {
                     getTypeGroup(s.getKey(), false, nameArr[1]);
                     return;
                  }
               }

               if (typeGroup.equals(MyConstant.TWO_USERS)) {
                  if (list.size() == 0) {
                     if (s != null && !scoreArr[0].trim().toString().equals("10")) {
                        if (isQuit_MyId)
                           typeMsg = LOOSE_MSG;
                        else
                           typeMsg = WIN_MSG;
                        if (!isGameMode)
                           typeMsg = null;
                        Log.e("isgmae", "isgmae1");
                        changeScreen();
                        finish();
                        return;
                     }
                  }
               } else {
                  // if (!isPlay) {
                  if (list.size() == 0 && !isGameMode) {
                     typeMsg = null;
                     if (nCount != 1) {
                        Log.e("isgmae", "isgmae6 " + nCount);
                        changeScreen();
                        finish();
                     }
                     return;
                  }
                  //}
               }
               if (typeGroup.equals(MyConstant.TWO_USERS)) {
                  if (s != null)
                     getTypeGroup(s.getKey(), false, nameArr[1]);
               }

               setData(list);
               checkUserAvailable();


            }
         }


         @Override
         public void onCancelled(DatabaseError databaseError) {
         }
      });

      Firebase.getInstance().getPoints(context).addValueEventListener(new ValueEventListener() {
         @Override
         public void onDataChange(DataSnapshot dataSnapshot) {
            if (dataSnapshot.getValue() != null)

               if (dataSnapshot.getValue().toString().equals(MyConstant.EMPTY_KEY)) {
                  //isPlay = false;

                  relativelayout_open_area.post(new Runnable() {
                     @Override
                     public void run() {
                        if (typeGroup.equals(MyConstant.TWO_USERS)) {
                           if (!isGameMode)
                              imageView.setVisibility(View.GONE);
                        } else {
                           if (imageView != null)
                              imageView.setVisibility(View.GONE);
                        }
                     }
                  });

                  Log.e("Before", "--------- " + pos + "---------" + timeArr.length);

                  if (pos == timeArr.length - 1) {
                     pos = -1;
                  }

                  pos++;

                  Log.e("After", "--------- " + pos);

                  relativelayout_open_area.postDelayed(new Runnable() {
                     @Override
                     public void run() {
                        if (imageView != null) imageView.setVisibility(View.VISIBLE);
                        addView();
                        if (imageView != null) imageView.setClickable(true);
                     }
                  }, timeArr[pos] * 1000);

               }

         }

         @Override
         public void onCancelled(DatabaseError databaseError) {

         }
      });

   }

   /**
    * not to start game until all players enters
    */
   private void checkUserAvailable() {
      switch (typeGroup) {
         case MyConstant.TWO_USERS:
            playerCount = 2;
            break;

         case MyConstant.THREE_USERS:
            playerCount = 3;
            break;

         case MyConstant.FOUR_USERS:
            playerCount = 4;
            break;

         case MyConstant.FIVE_USERS:
            playerCount = 5;
            break;

         case MyConstant.SIX_USERS:
            playerCount = 6;
            break;

      }

      if (playerCount == list.size() && dialog != null) {
         totalGames = MySharedPreference.getInstance().getTotalGames(context);
         if (isStartGame) {
            if (isMyId) {
               totalGames++;
            }
            //isPlay = true;
            //isGameMode = false;
            MySharedPreference.getInstance().saveTotalGames(context, totalGames);
            if (myDialogs == null) myDialogs = new MyDialogs();
            if (!isFinishing())
               myDialogs.countDownDialod(context, this);
         }
         isGameMode = true;
         isStartGame = false;
         dialog.dismiss();
         isCount = playerCount == list.size();
      }


   }


   public void hit() {
      Task<Void> query = Firebase.getInstance().bidForPoint(context);
      query.addOnCompleteListener(new OnCompleteListener<Void>() {
         @Override
         public void onComplete(Task<Void> task) {
            if (task.isSuccessful()) {
               totalpoints++;
               Firebase.getInstance().setPoint(context, totalpoints, status);
               refreshScoreField();
               MyUtil.stopMedia();
            }
         }
      });
   }

   public void refreshScoreField() {
      Firebase.getInstance().setEmptyPoint(context);
   }

   private void setUpIds() {
      mGridView = (GridView) findViewById(R.id.grid);
      relativelayout_open_area = (RelativeLayout) findViewById(R.id.relativelayout_open_area);
      ViewTreeObserver vto = relativelayout_open_area.getViewTreeObserver();
      vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

         public void onGlobalLayout() {
            ViewTreeObserver obs = relativelayout_open_area.getViewTreeObserver();
            if (android.os.Build.VERSION.SDK_INT < 16)
               obs.removeGlobalOnLayoutListener(this);
            else
               obs.removeOnGlobalLayoutListener(this);

            w = relativelayout_open_area.getWidth();
            h = relativelayout_open_area.getHeight();
         }
      });
   }

   public void addView() {

      imageView.setBackgroundResource(objects[pos]);
      imageView.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
            MyUtil.playMedia(context, 4, false);
            imageView.setClickable(false);
            status = MyConstant.ONLINE;
            relativelayout_open_area.post(new Runnable() {
               @Override
               public void run() {
                  //isPlay = false;
                  hit();
               }
            });

         }
      });

      Random rn = new Random();
      int range = w - imageView.getWidth() - 400;
      int leftMargin = rn.nextInt(range);

      Random rn1 = new Random();
      int range1 = h - 500;
      int topMargin = rn1.nextInt(range1);
      setMargins(imageView, leftMargin, topMargin, 0, 0);


   }

   private void setMargins(View view, int left, int top, int right, int bottom) {
      if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
         ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
         p.setMargins(left, top, right, bottom);
         view.requestLayout();
      }
   }


   private void setData(ArrayList<HashMap<String, String>> list) {

      for (int i = 0; i < list.size(); i++) {

         if (userId.equals(list.get(i).get(MyConstant.USER_ID))) {
            list.remove(i);
            map = new HashMap<>();
            map.put(MyConstant.USER_ID, userId);
            map.put(MyConstant.NAME, ownName);
            list.add(0, map);
            break;
         }
      }

      playerInfoAdapter = new PlayerInfoAdapter(list);
      mGridView.setAdapter(playerInfoAdapter);

   }

   private void getThirdCompletion() {
      totalCompleteGame = MySharedPreference.getInstance().getThirdComplete(context);
      totalCompleteGame++;
      MySharedPreference.getInstance().setThirdComplete(context, totalCompleteGame);
   }

   /**
    * type group for checking players
    */
   private void getTypeGroup(String userIdKey, boolean checkOffLine, String status) {

      switch (typeGroup) {

         case MyConstant.TWO_USERS:
            // if any oneone score 10 first then finish the game
            if (scoreArr[0].trim().toString().equals("10")) {
               isWinGame = true;

               if (isWinGame && userId.trim().toString().equalsIgnoreCase(userIdKey)) {
                  typeMsg = WIN_MSG;
                  winGame = MySharedPreference.getInstance().getWinGames(context);
                  winGame++;
                  MySharedPreference.getInstance().saveWinGame(context, winGame);
               } else {
                  typeMsg = LOOSE_MSG;
               }
               HIDE_ADS = true;
               getThirdCompletion();
               isOnPause = false;
               Firebase.getInstance().removeGroup(context);
               changeScreen();
               finish();
               return;
            }
            // if any one quit while two players are playing
            if (isQuit) {
               // if quit by me
               if (isQuit_MyId) {
                  typeMsg = LOOSE_MSG;
               } else {
                  typeMsg = WIN_MSG;
                  winGame = MySharedPreference.getInstance().getWinGames(context);
                  winGame++;
                  MySharedPreference.getInstance().saveWinGame(context, winGame);
               }
               Firebase.getInstance().removeGroup(context);
               TYPE_SCREEN = true;
               isOnPause = false;
               changeScreen();
               finish();
               return;
            }

            break;

         case MyConstant.THREE_USERS:
         case MyConstant.FOUR_USERS:
         case MyConstant.FIVE_USERS:
         case MyConstant.SIX_USERS:
            // if any oneone score 10 first then finish the game
            if (scoreArr[0].trim().toString().equals("10")) {
               isWinGame = true;
               boolean isWin = false;

               if (isWinGame && userId.trim().toString().equalsIgnoreCase(userIdKey)) {
                  isWin = true;
                  typeMsg = WIN_MSG;
                  winGame = MySharedPreference.getInstance().getWinGames(context);
                  winGame++;
                  MySharedPreference.getInstance().saveWinGame(context, winGame);
               } else {
                  isWin = false;
                  typeMsg = LOOSE_MSG;
               }
               HIDE_ADS = true;
               isOnPause = false;
               mCount++;

               if (isGameMode) {
                  getThirdCompletion();
                  isPlay = true;
                  MyUtil.stopMediaCopy();
                  //isCount = true;
                  try {
                     if (mCount == 1) {
                        isGameMode = true;
                        myDialogs.winLossDialog(this, this, isWin);
                        if (isWin)
                           MyUtil.playMediaCopy(context, 2, false);
                        else
                           MyUtil.playMediaCopy(context, 5, false);
                        Firebase.getInstance().removeGroup(context);
                     }
                  } catch (Exception e) {
                     e.printStackTrace();
                  }

                  return;
               }

            }

            if (checkOffLine) {
               for (int i = 0; i < list.size(); i++) {
                  String idKey = list.get(i).get(MyConstant.USER_ID).trim();
                  if (userIdKey.equalsIgnoreCase(idKey) && status.trim().equals(MyConstant.OFFLINE)) {
                     list.remove(i);
                     Firebase.getInstance().removeUser(context, userIdKey);
                     if (playerInfoAdapter != null)
                        playerInfoAdapter.notifyDataSetChanged();
                     size--;
                     break;
                  }


               }
            }

            if (size == 1) {
               isGameFinish = true;

               if (isQuit_MyId) {
                  typeMsg = LOOSE_MSG;
               } else {
                  typeMsg = WIN_MSG;
                  winGame = MySharedPreference.getInstance().getWinGames(context);
                  winGame++;
                  MySharedPreference.getInstance().saveWinGame(context, winGame);
               }
               isOnPause = false;
               Firebase.getInstance().removeGroup(context);
               changeScreen();
               finish();
            }
            break;
      }
   }

   /**
    * swith to play screen while win loss game
    */
   private void changeScreen() {
      //isGameMode = false;
      // nCount = 0;
      MyUtil.stopMediaCopy();
      myDialogs.cancelDialog();
      Intent intent = new Intent(context, MainActivity.class);
      intent.putExtra(TYPE_MESSAGE, typeMsg);
      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
      startActivity(intent);
   }

   @Override
   public void onSuccess(boolean isReq) {
      if (isReq) {
         relativelayout_open_area.post(new Runnable() {
            @Override
            public void run() {
               if (imageView != null)
                  imageView.setVisibility(View.VISIBLE);
            }
         });

         MyUtil.playMediaCopy(context, 1, true);
      } else {
         typeMsg = null;
         changeScreen();
         finish();
      }
   }

   @Override
   public void onFail() {
      MyUtil.stopMediaCopy();
   }
}
