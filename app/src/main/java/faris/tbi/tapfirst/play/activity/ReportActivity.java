package faris.tbi.tapfirst.play.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import faris.tbi.tapfirst.R;
import faris.tbi.tapfirst.myUtilities.MySharedPreference;
import faris.tbi.tapfirst.myUtilities.views.RobotoTextView;

/**
 * Created by brst-pc81 on 12/26/16.
 */

public class ReportActivity extends Fragment {
    private int winGames = 0, looseGames = 0, totalGames = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.report_view, container, false);
        inItView(view);
        return view;
    }

    private void inItView(View view) {
        totalGames = MySharedPreference.getInstance().getTotalGames(getActivity());
        winGames = MySharedPreference.getInstance().getWinGames(getActivity());
        looseGames = totalGames - winGames;
        //looseGames = MySharedPreference.getInstance().getLooseGames(this);//totalGames - winGames;

        ((RobotoTextView) view.findViewById(R.id.tv_won)).setText(winGames > 9 ? "" + winGames : "0" + winGames);
        ((RobotoTextView) view.findViewById(R.id.tv_loose)).setText(looseGames > 9 ? "" + looseGames : "0" + looseGames);
        ((RobotoTextView) view.findViewById(R.id.tv_total_played)).setText(totalGames > 9 ? "" + totalGames : "0" + totalGames);

        //MyConstant.IS_MEDIA_CHANGED = false;
        //MyUtil.playMedia(this, 0, true);
    }


}
